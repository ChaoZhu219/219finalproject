package controller;

import buzzword.Template;
import data.GameData;
import data.UserList;
import gui.Dialog;
import javafx.animation.AnimationTimer;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import javax.xml.stream.events.Characters;
import java.io.IOException;
import java.util.Random;


public class Controller {
    private Template template;

    private boolean pauseFlag = false;
    private boolean startFlag = false;
    private boolean gameoverFlag = false;
    private boolean showLoginFlag = false;
    private boolean showModeFlag = false;
    private int timerCount = 0;
    private static final int timeLong = 60;
    private int initTimeRemain = timeLong;
    private int levelX = 0;
    private int levelY = 0;
    private boolean ctrlPressed = false;
    private boolean shiftPressed = false;
/********** Switch **********/
    public Controller(Template template) {
        this.template = template;
        pauseFlag = false;
    }

    public void start() throws IOException {
        //template.setUserList(template.getFileManager().loadUserList());
        template.setUserList(template.getFileManager().loadUserList(template.getUserList()));
        if(template.getUserList() == null){
            UserList userList = new UserList();
            template.setUserList(userList);
            template.getUserList().createNewUserList(template);
        }
    }

    public void handleIfExit(){
        handlePause();


        BorderPane ifExitRoot = new BorderPane();
        Scene ifExitScene = new Scene(ifExitRoot, 500, 300);
        ifExitScene.getStylesheets().add("css/buzzword.css");
        ifExitRoot.getStyleClass().add("exit-background");
        Stage ifExitStage = new Stage();
        ifExitStage.setScene(ifExitScene);
        ifExitStage.initStyle(StageStyle.TRANSPARENT);
        ifExitStage.show();

    }

    public void handleExit(){
        Dialog dialog = new Dialog();
        dialog.showDialog();
    }

    public void handleExitPause(){
        handlePause();
        Dialog dialog = new Dialog();
        dialog.showDialog();
    }

    public void handleCreate() {
        template.getDisplayGUI().getPrivateStage().setScene(template.getDisplayGUI().getSceneCreate());
    }

    public void end(){

    }

    public void handleHome() {
        template.getDisplayGUI().getPrivateStage().setScene(template.getDisplayGUI().getSceneHome());
    }

    public void handleLogin() {
        if(!showLoginFlag){
            template.getDisplayGUI().getLoginStack().getChildren().add(template.getDisplayGUI().getLoginPane());
            showLoginFlag = true;
        }
    }

    public void handlePause() {
        if(pauseFlag){
            template.getDisplayGUI().getPauseButton().getStyleClass().set(0, "pause-button");
            template.getDisplayGUI().getLetterStack().getChildren().remove(template.getDisplayGUI().getShell());
            pauseFlag = false;
        }
        else{
            template.getDisplayGUI().getPauseButton().getStyleClass().set(0, "unpause-button");
            template.getDisplayGUI().getLetterStack().getChildren().add(template.getDisplayGUI().getShell());
            pauseFlag = true;
        }
    }

    public void playTimer(){
        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                // timer
                if(!pauseFlag) {
                    if(timerCount == 60){
                        timerCount = 0;
                    }
                    timerCount++;
                    if (timerCount == 60) {
                        template.getDisplayGUI().getClock().setText(Integer.toString(--initTimeRemain) + "  ");
                    }
                }
                if(initTimeRemain == 0){
                    timerCount = 0;
                    initTimeRemain = timeLong;
                    pauseFlag = false;
                    startFlag = false;
                    gameoverFlag = true;
                    showSolution();
                    stop();
                }
                // type
                template.getDisplayGUI().getScenePlay().setOnKeyTyped((KeyEvent event) -> {
                    char c = event.getCharacter().charAt(0);
                    boolean breakFlag = false;
                    for(int j = 0; j < 4; j++){
                        for(int i = 0; i < 4; i++){
                            char L = template.getDisplayGUI().getLetters()[i][j].charAt(0);
                            char l = template.getDisplayGUI().getLetters()[i][j].toLowerCase().charAt(0);
                            if(c == L || c == l){
                                if(!template.getDisplayGUI().getLetterChoseFlag()[i][j]) {
                                    handleLetterChoose(i, j);
                                    breakFlag = true;
                                }
                            }
                            if(breakFlag) break;
                        }
                        if(breakFlag) break;
                    }
                    if(c == ' '){
                        handleLetterMatrixMouseReleased();
                        breakFlag = true;
                    }
                    if(!breakFlag){
                        reinitializeLettersChoose();
                    }
                });


            }
            @Override
            public void stop(){
                super.stop();
                end();
            }
        };
        timer.start();
    }

    private void showSolution() {
        Stage solutionStage = new Stage();
        VBox infoBox = new VBox();
        BorderPane root = new BorderPane();
        Scene solutionScene = new Scene(root);
        solutionStage.setScene(solutionScene);
        infoBox.setAlignment(Pos.CENTER);
        infoBox.setSpacing(5);
        GameData tempGD = template.getGameData();
        Button closeSolution = new Button("X");
        HBox toolbar = new HBox();
        toolbar.setAlignment(Pos.CENTER_RIGHT);
        toolbar.getChildren().add(closeSolution);
        infoBox.getChildren().addAll(new Label("Personal Best in this level : "
                + tempGD.getPersonalBest()[tempGD.getCurrentMode()][tempGD.getCurrentLevel()]),
                new Label("All solutions : "));
        for(int i = 0; i < template.getDisplayGUI().getValidWordListSave().size(); i++){
            infoBox.getChildren().add(new Label(template.getDisplayGUI().getValidWordListSave().get(i)));
        }
        root.setTop(toolbar);
        root.setCenter(infoBox);
        root.setMinSize(500, 300);
        solutionStage.initStyle(StageStyle.TRANSPARENT);            //no border
        closeSolution.setOnMouseClicked(e -> {solutionStage.close();});
        solutionStage.show();
    }


    public void handleCancel() {
        template.getDisplayGUI().getLoginStack().getChildren().clear();
        showLoginFlag = false;
    }

    public void handleHelp() {
        template.getDisplayGUI().getPrivateStage().setScene(template.getDisplayGUI().getSceneHelp());
    }

    public void handleStart() {

        for(int i = 0; i < 4; i++){
            for(int j = 0; j < 2; j++){
                if(i > 0 || j > 0){
                    if(template.getGameData().getModeLevelFlag()[template.getGameData().getCurrentMode()][4 * j + i]){
                        template.getDisplayGUI().getLevelCell()[i][j].getChildren().get(1).setVisible(false);
                    }
                    else{
                        template.getDisplayGUI().getLevelCell()[i][j].getChildren().get(1).setVisible(true);
                    }
                }
            }
        }



        template.getDisplayGUI().getPrivateStage().setScene(template.getDisplayGUI().getSceneLevel());
    }

    public void handleLogout() {
        template.getDisplayGUI().getSceneHome().setRoot(template.getDisplayGUI().getHomePane());
        template.getDisplayGUI().getSceneCreate().setRoot(template.getDisplayGUI().getCreatePane());
        template.getDisplayGUI().getModeBox()[template.getGameData().getCurrentMode()].getChildren().get(1).getStyleClass().set(0, "mode-label");
        template.getGameData().setCurrentMode(0);
        template.getDisplayGUI().getModeBox()[template.getGameData().getCurrentMode()].getChildren().get(1).getStyleClass().set(0, "mode-picked-label");
        template.getDisplayGUI().getModePane().getChildren().clear();

        template.getDisplayGUI().getModeLabel().setText("Mode : " + template.getGameData().getModes()[0]);
        template.getDisplayGUI().getLevelPane().getStyleClass().set(0, "level-background0");
        for(int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if(i > 0 || j > 0){
                    template.getDisplayGUI().getLevelCell()[i][j].getChildren().remove(1);
                    template.getDisplayGUI().getLevelCell()[i][j].getChildren().add(template.getDisplayGUI().getLevelLockImageView()[0][i][j]);
                }
            }
        }

        showModeFlag = false;

    }

    public void handleRestart(){
        int j = template.getGameData().getCurrentLevel() - 1;
        int i = j % 4;
        j = j / 4;
        initTimeRemain = timeLong;
        handlePlay(i, j);
    }

    public void handlePlay(int i, int j) {
        if(!template.getGameData().getModeLevelFlag()[template.getGameData().getCurrentMode()][j * 4 + i]) {
            Dialog dialog = new Dialog();
            dialog.showDialog("This level is locked !");
            return;
        }
        template.getDisplayGUI().getPrivateStage().setScene(template.getDisplayGUI().getScenePlay());
        gameoverFlag = false;
        initTimeRemain = timeLong;
        if(!startFlag){
            playTimer();
            startFlag = true;
        }
        levelX = i;
        levelY = j;
        // set next button state
        if(j * 4 + i <= 14){
            if(!template.getGameData().getModeLevelFlag()[template.getGameData().getCurrentMode()][j * 4 + i + 1]){
                template.getDisplayGUI().getNextButton().setDisable(true);
            }
            else{
                template.getDisplayGUI().getNextButton().setDisable(false);
            }
        }
        else{
            template.getDisplayGUI().getNextButton().setDisable(true);
        }

        template.getDisplayGUI().getLevelDisplay().setText(Integer.toString(j * 4 + i + 1));
        template.getGameData().setCurrentLevel(j * 4 + i + 1);
        // need generate new grid letters

        template.getDisplayGUI().setTotalScoreInt(0);
        template.getDisplayGUI().getTotalScore().setText("0");
        template.getDisplayGUI().getDiscoveredBox().getChildren().clear();

        reinitializeLettersChoose();
        generateLetterGrid();

    }

    public void handleNextLevel(){
        initTimeRemain = timeLong;
        if(levelX < 3){
            handlePlay(levelX + 1, levelY);
        }
        else{
            handlePlay(0, levelY + 1);
        }

    }

    private void reinitializeLettersChoose(){
        template.getDisplayGUI().getCurrentWordLabel().setText("");
        for(int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                template.getDisplayGUI().getLetterChoseFlag()[i][j] = false;
                template.getDisplayGUI().getLetterCell()[i][j].getChildren().get(0).setVisible(true);
            }
        }
    }

    private void generateLetterGrid(){
        Random rand = new Random();
        boolean[][] tempFlag = new boolean[][]{{false, false, false, false}, {false, false, false, false},
                                               {false, false, false, false}, {false, false, false, false}};
        String tempStr = "";

        while(tempStr.length() != template.getGameData().getCurrentLevel() + 2){
            tempStr = template.getLevelContent().getValidWords()[rand.nextInt(template.getLevelContent().getValidWords().length)];
        }

        System.out.println(tempStr);

        int count = tempStr.length();
        while(count > 0){
            int i = rand.nextInt(4);
            int j = rand.nextInt(4);
            if(!tempFlag[i][j]){
                tempFlag[i][j] = true;
                String s = (tempStr.substring(count - 1, count));
                String str = "images/letter" + s + ".png";
                template.getDisplayGUI().getLetters()[i][j] = s;
                template.getDisplayGUI().getLetterCell()[i][j].getChildren().remove(1);
                template.getDisplayGUI().getLetterCell()[i][j].getChildren().add(new ImageView(new Image(str, 100, 100, false, true)));
                count--;
            }
        }

        for(int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if(!tempFlag[i][j]){
                    template.getDisplayGUI().getLetterCell()[i][j].getChildren().remove(1);
                    char c = randomCharByRate(rand.nextInt(100000));
                    String str = "images/letter" + c + ".png";
                    template.getDisplayGUI().getLetterCell()[i][j].getChildren().add(new ImageView(new Image(str, 100, 100, false, true)));
                    template.getDisplayGUI().getLetters()[i][j] = Character.toString(c);

                    //template.getDisplayGUI().getLetterCell()[i][j].getChildren().add(new ImageView(new Image("images/letter .png", 100, 100, false, true))); //??  test
                }
            }
        }

        template.getDisplayGUI().setTargetScoreInt(validTargetWordDetect());
    }

    private char randomCharByRate(int i){
        int rate = 8167;
        if(i <= rate) return 'A';
        rate += 1492;
        if(i <= rate) return 'B';
        rate += 2782;
        if(i <= rate) return 'C';
        rate += 4253;
        if(i <= rate) return 'D';
        rate += 12702;
        if(i <= rate) return 'E';
        rate += 2228;
        if(i <= rate) return 'F';
        rate += 2015;
        if(i <= rate) return 'G';
        rate += 6049;
        if(i <= rate) return 'H';
        rate += 6966;
        if(i <= rate) return 'I';
        rate += 153;
        if(i <= rate) return 'J';
        rate += 772;
        if(i <= rate) return 'K';
        rate += 4025;
        if(i <= rate) return 'L';
        rate += 2406;
        if(i <= rate) return 'M';
        rate += 6749;
        if(i <= rate) return 'N';
        rate += 7507;
        if(i <= rate) return 'O';
        rate += 1929;
        if(i <= rate) return 'P';
        rate += 95;
        if(i <= rate) return 'Q';
        rate += 5987;
        if(i <= rate) return 'R';
        rate += 6327;
        if(i <= rate) return 'S';
        rate += 9056;
        if(i <= rate) return 'T';
        rate += 2758;
        if(i <= rate) return 'U';
        rate += 978;
        if(i <= rate) return 'V';
        rate += 2360;
        if(i <= rate) return 'W';
        rate += 150;
        if(i <= rate) return 'X';
        rate += 1974;
        if(i <= rate) return 'Y';
        rate += 74;
        if(i <= rate) return 'Z';
        return 'E';
    }

    private int validTargetWordDetect(){
        int totalScore;
        template.getDisplayGUI().getValidWordList().clear();
        template.getDisplayGUI().getValidWordListSave().clear();
        if (template.getGameData().getCurrentMode() == 0){
            totalScore = noPathMode();
        }
        else{
            totalScore =  pathMode();
        }
        template.getDisplayGUI().setTargetScoreInt(totalScore);
        template.getDisplayGUI().getTargetScore().setText(Integer.toString(totalScore));
        return totalScore;
    }

    private int noPathMode(){
        int totalScore = 0;
        for(int i = 0; i < template.getLevelContent().getValidWords().length; i++){
            String tempWord = template.getLevelContent().getValidWords()[i];
            boolean[] flag = new boolean[tempWord.length()];
            for(int j = 0; j < tempWord.length(); j++){
                flag[j] = false;
            }
            for(int j = 0; j < 4; j++){
                for(int k = 0; k < 4; k++){
                    for(int l = 0; l < tempWord.length(); l++){
                        if(!flag[l]){
                            if(template.getDisplayGUI().getLetters()[j][k].equals(Character.toString(tempWord.charAt(l)))){
                                flag[l] = true;
                                break;
                            }
                        }
                    }
                }
            }
            boolean validFlag = true;
            for(int j = 0; j < tempWord.length(); j++){
                if(flag[j] == false) validFlag = false;
            }
            if(validFlag){
                template.getDisplayGUI().getValidWordList().add(tempWord);
                template.getDisplayGUI().getValidWordListSave().add(tempWord);
                System.out.println(tempWord);
                totalScore += 10;
            }
        }
        return totalScore;
    }

    private int pathMode(){
        int totalScore;
        for(int i = 0; i < 4; i++){
            for(int j = 0; j < 4; j++){
                boolean[][] flag = new boolean[4][4];
                for(int x = 0; x < 4; x++){
                    for(int y = 0; y < 4; y++){
                        flag[x][y] = false;
                    }
                }
                String str = "";
                recursionWord(i, j, str, flag);
            }
        }
        totalScore = 10 * template.getDisplayGUI().getValidWordList().size();
        return totalScore;
    }

    private void recursionWord (int i, int j, String str, boolean[][] flag){
        if(flag[i][j]) return;
        else flag[i][j] = true;
        str += template.getDisplayGUI().getLetters()[i][j];
        if(str.length() >= 3){
            if(template.getLevelContent().getWordDictionary()[template.getGameData().getCurrentMode()].contains(str.toLowerCase())){
                if(!template.getDisplayGUI().getValidWordList().contains(str)){
                    template.getDisplayGUI().getValidWordList().add(str);
                    template.getDisplayGUI().getValidWordListSave().add(str);
                    System.out.println(str);
                }
            }
        }
        if(str.length() >= 16) return;
        if(i > 0) recursionWord(i - 1, j, str, flag);
        if(i < 3) recursionWord(i + 1, j, str, flag);
        if(j > 0) recursionWord(i, j - 1, str, flag);
        if(i < 0) recursionWord(i, j + 1, str, flag);
        if(i > 0 && j > 0) recursionWord(i - 1, j - 1, str, flag);
        if(i > 0 && j < 3) recursionWord(i - 1, j + 1, str, flag);
        if(i < 3 && j > 0) recursionWord(i + 1, j - 1, str, flag);
        if(i < 3 && j < 3) recursionWord(i + 1, j + 1, str, flag);
    }



    public void handleLetterChoose(int i, int j) {
        if(gameoverFlag){
            reinitializeLettersChoose();
            return;
        }
        //if(!mousePressedFlag) return;
        if(template.getDisplayGUI().getLetterChoseFlag()[i][j]){
            template.getDisplayGUI().getLetterChoseFlag()[i][j] = false;
            template.getDisplayGUI().getLetterCell()[i][j].getChildren().get(0).setVisible(true);
            StringBuffer strB = new StringBuffer(template.getDisplayGUI().getCurrentWordLabel().getText());
            strB.replace(strB.lastIndexOf(template.getDisplayGUI().getLetters()[i][j]), strB.lastIndexOf(template.getDisplayGUI().getLetters()[i][j]) + 1, "");
            template.getDisplayGUI().getCurrentWordLabel().setText(strB.toString());

        }
        else{
            template.getDisplayGUI().getLetterChoseFlag()[i][j] = true;
            template.getDisplayGUI().getLetterCell()[i][j].getChildren().get(0).setVisible(false);
            //add into current word
            template.getDisplayGUI().getCurrentWordLabel().setText(template.getDisplayGUI().getCurrentWordLabel().getText()
                    + template.getDisplayGUI().getLetters()[i][j]);

        }
        // need add generate current words

    }

    public void handleLetterMatrixMouseReleased() {
        if(!gameoverFlag){
            String str = template.getDisplayGUI().getCurrentWordLabel().getText();
            for(int k = 0; k < template.getDisplayGUI().getValidWordList().size(); k++){
                if(template.getDisplayGUI().getValidWordList().get(k).equals(str)){
                    template.getDisplayGUI().getValidWordList().remove(k);
                    int scoreAdd = scoreCalculate(str);
                    int totalScore = template.getDisplayGUI().getTotalScoreInt() + scoreAdd;
                    template.getDisplayGUI().setTotalScoreInt(totalScore);
                    template.getDisplayGUI().getTotalScore().setText(Integer.toString(totalScore));
                    addDiscoverWord(str, scoreAdd);
                    break;
                }
            }
        }
        reinitializeLettersChoose();
    }

    private int scoreCalculate(String str){
        return 7 + str.length();  ///??? 10 可以换为相应分数
    }

    private void addDiscoverWord(String str, int scoreAdd){
        Label word = new Label(str);
        word.getStyleClass().add("discover-word-label");
        Label score = new Label(Integer.toString(scoreAdd));
        score.getStyleClass().add("discover-word-label");
        HBox wordBox = new HBox();
        wordBox.setAlignment(Pos.CENTER);
        wordBox.setSpacing(20);
        wordBox.getChildren().addAll(word, score);
        template.getDisplayGUI().getDiscoveredBox().getChildren().add(wordBox);
    }

    public void handleModePick() {
        if(!showModeFlag) {
            template.getDisplayGUI().getModePane().getChildren().addAll(template.getDisplayGUI().getModeBox());
            showModeFlag = true;
        }
        else {
            template.getDisplayGUI().getModePane().getChildren().clear();
            showModeFlag = false;
        }
    }

    public void handleMode(int modeIndex) {
        template.getDisplayGUI().getModeBox()[template.getGameData().getCurrentMode()].getChildren().get(1).getStyleClass().set(0, "mode-label");
        template.getGameData().setCurrentMode(modeIndex);
        template.getDisplayGUI().getModeBox()[template.getGameData().getCurrentMode()].getChildren().get(1).getStyleClass().set(0, "mode-picked-label");
        template.getDisplayGUI().getModeLabel().setText("Mode : " + template.getGameData().getModes()[template.getGameData().getCurrentMode()]);
        //template.getDisplayGUI().getLevelPane().getStyleClass().remove(0);
        template.getDisplayGUI().getLevelPane().getStyleClass().set(0, "level-background" + Integer.toString(modeIndex));
        //// need also change lock gif
        for(int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if(i > 0 || j > 0){
                    template.getDisplayGUI().getLevelCell()[i][j].getChildren().remove(1);
                    template.getDisplayGUI().getLevelCell()[i][j].getChildren().add(template.getDisplayGUI().getLevelLockImageView()[template.getGameData().getCurrentMode()][i][j]);
                }
            }
        }
    }

    public void handleSummit(){
        Dialog dialog = new Dialog();
        String name = template.getDisplayGUI().getUserNameText().getText();
        String password = template.getDisplayGUI().getPasswordText().getText();
        for(int i = 0; i < template.getUserList().getNumberOfUser(); i++){
            if(template.getUserList().getUserName().get(i).equals(name)){
                if(template.getUserList().getUserPassword().get(i).equals(password)){
                    template.getDisplayGUI().getSceneHome().setRoot(template.getDisplayGUI().getHomePaneLogin());

                    //load game data and load profile
                    dialog.showDialog("Congratulations on your successful registration !");
                    try{
                        template.setGameData(template.getFileManager().loadUserData(template.getGameData(), name));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                    template.getDisplayGUI().getSceneCreate().setRoot(template.getDisplayGUI().getCreatePaneLogin());
                    // need reload profile
                    //template.getDisplayGUI().reinitializeProfile();
                    for(int j = 0; j < 6; j++){
                        for(int k = 0; k < 8; k++){
                            if(k != 0){
                                if(template.getGameData().getModeLevelFlag()[j][k]){
                                    template.getDisplayGUI().getProfileMatrixStackPane()[j][k].getChildren().get(1).setVisible(false);
                                    //template.getDisplayGUI().getProfileMatrixStackPane()[j][k].getChildren().remove(1);
                                }
                                else{
                                    template.getDisplayGUI().getProfileMatrixStackPane()[j][k].getChildren().get(1).setVisible(true);
                                }
                            }

                        }
                    }
                    template.getDisplayGUI().getSceneHome().setRoot(template.getDisplayGUI().getHomePaneLogin());
                    template.getDisplayGUI().resetLoginLabels(name);

                    return;
                }
                else{
                    dialog.showDialog("The Password is wrong !");
                    return;
                }

            }
        }
        dialog.showDialog("User doesn't exist !");
        return;

        //template.getDisplayGUI().getPrivateStage().setScene(template.getDisplayGUI().getScenePlay());

    }

    public void handleSignUp() {
        Dialog dialog = new Dialog();
        String nameTF = template.getDisplayGUI().getNameTF().getText();
        String passN = template.getDisplayGUI().getPassNew().getText();
        String passC = template.getDisplayGUI().getPassCon().getText();
    // input check
        if(nameTF.length() < 4 || nameTF.length() > 15){
            dialog.showDialog("The length of the user name should be between 4 and 15 !");
            return;
        }
        if(passN.length() < 4 || passN.length() > 15){
            dialog.showDialog("The length of the password should be between 4 and 15 !");
            return;
        }
        if(!passN.equals(passC)){
            dialog.showDialog("The password entered twice does not match !");
            return;
        }
        for(int i = 0; i < nameTF.length(); i++){
            if(!(Character.isAlphabetic(nameTF.charAt(i)) || Character.isDigit(nameTF.charAt(i)))){
                dialog.showDialog("The user name can only contain letters and numbers !");
            }
        }
        for(int i = 0; i < passN.length(); i++){
            if(!(Character.isAlphabetic(passN.charAt(i)) || Character.isDigit(passN.charAt(i)))){
                dialog.showDialog("The password can only contain letters and numbers !");
            }
        }
        for(int i = 0; i < template.getUserList().getNumberOfUser(); i++){
            if(template.getUserList().getUserName().get(i).equals(nameTF)){
                dialog.showDialog("User already exist !");
                return;
            }
        }
        try{
            template.getUserList().addNewUser(nameTF, passN);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    // change create scene and home scene to login pane
        dialog.showDialog("Congratulations on your successful registration !");
        template.getDisplayGUI().getUserNameLabelCreate().setText(nameTF);
        template.getDisplayGUI().getSceneCreate().setRoot(template.getDisplayGUI().getCreatePaneLogin());
        template.getDisplayGUI().getSceneHome().setRoot(template.getDisplayGUI().getHomePaneLogin());
        try{
            template.getGameData().createNewGameData(template, nameTF);
        }
        catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void handelHomeKeyShortcut(boolean b, KeyEvent e) {
        if(e.getCode().equals(KeyCode.CONTROL)){
            ctrlPressed = b;
        }
        if(e.getCode().equals(KeyCode.SHIFT)){
            shiftPressed = b;
        }
        if(e.getCode().equals(KeyCode.L)){
            if(ctrlPressed){
                handleLogin();
            }
        }
        if(e.getCode().equals(KeyCode.N)){
            if(ctrlPressed){
                handleCreate();
            }
        }
        if(e.getCode().equals(KeyCode.Q)){
            if(ctrlPressed){
                handleExit();
            }
        }
    }




/*
    public HBox initModel(String image, String text){
        HBox hbox = new HBox();
        Label label = new Label(text);
        label.getStyleClass().add("mode-label");
        hbox.getChildren().addAll(new ImageView(new Image(image, 178, 100, false, true)), label);
        hbox.llllacing(30);
        return hbox;
    }
*/

}
