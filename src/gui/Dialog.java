package gui;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Created by peter on 2016/11/25.
 */
public class Dialog {
    private Stage stage;
    private Scene scene;
    private VBox root;
    private Label label;
    private Button button;


    public Dialog(){
        initialGUI();
    }

    public void initialGUI(){
        root = new VBox();
        root.setAlignment(Pos.CENTER);
        root.setMinSize(400, 225);
        root.setSpacing(50);
        scene = new Scene(root);
        label = new Label();
        button = new Button();

        root.getChildren().addAll(label, button);
        stage = new Stage();
        stage.setScene(scene);
    }

    public void showDialog(String info){
        button.setOnAction(e -> stage.close());
        button.setText("OK");
        label.setText(info);
        stage.show();
    }
    public void showDialog(){
        button.setOnAction(e -> quitGame());
        button.setText("Quit !");
        label.setText("Do you really want to quit the game?");
        stage.show();
    }
    public void quitGame(){
        System.exit(0);
    }

}
