package gui;

import buzzword.Template;
import controller.Controller;
import javafx.animation.PathTransition;
import javafx.animation.Timeline;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.CubicCurveTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by peter on 2016/10/30.
 */
public class DisplayGUI {
/********** Member Variable **********/
    Template template;
    private int appSpecificWindowWidth;
    private int appSpecificWindowHeight;
    private Button exit;
/********** Display **********/
    private Stage primaryStage;
    private Scene sceneHome;
    private Scene sceneCreate;
    private Scene sceneHelp;
    private Scene sceneLevel;
    private Scene scenePlay;
/********** home **********/
    private BorderPane homePane;
    private Button loginButton;
    private Button createButton;
    private Button quitHomeButton;
    //
    private BorderPane homePaneLogin;
    private Label userNameLabelHome;
    private Button modeButton;
    private Button startButton;
    private Button helpButton;
    private Button logoutButton;
    private Button quitHomeLoginButton;
    private HBox homeLoginWorkspace;
    //
    private VBox modePane;
    private HBox modeBox[];
    private StackPane loginPane;
    private StackPane loginStack;
    private TextField userNameText;
    private PasswordField passwordText;
    private Button summitButton;
    private Button cancelButton;
/********** create **********/
    private BorderPane createPane;
    private Button createToHomeButton;
    private Button quitCreateButton;
    private Button createLoginToLevelButton;
    private TextField nameTF;
    private PasswordField passNew;
    private PasswordField passCon;
    private Label singUp;
    private Label userNameLabelCreate;
    private Button createLoginToHomeButton;
    private Button quitCreateLoginButton;
    private BorderPane createPaneLogin;
    private StackPane[][] profileMatrixStackPane;
    //...
/********** level **********/
    private BorderPane levelPane;
    private Label userNameLabelLevel;
    private Label modeLabel;
    private Button levelToHomeButton;
    private Button profileButton;
    private Button quitLevelButton;
    private StackPane levelStack;
    private GridPane levelMatrix;
    private StackPane[][] levelCell;
/********** help **********/
    private BorderPane helpPane;
    private FlowPane informationPane;
    private Label userNameLabelHelp;
    private Button helpToHomeButton;
    private Button quitHelpButton;
/********** play **********/
    private BorderPane playPane;
    private Button playToHomeButton;
    private Button restartButton;
    private Button nextButton;
    private Button pauseButton;
    private Button quitPlayButton;
    private GridPane letterMatrix;
    private StackPane letterStack;
    private ImageView shell = new ImageView(new Image("images/shell.jpg", 520, 520, false, true));
    private StackPane[][] letterCell;
    private ImageView[][] letterCellImageView;
    private String[][] letters;
    private boolean[][] letterChoseFlag;
    private ImageView[][][] levelLockImageView;
    private HBox currentWordBox;
    private VBox discoveredBox;
    private HBox totalBox;
    private HBox targetBox;
    private Label clock;
    private Label levelDisplay;
    private Label currentWordLabel;
    private Label totalScore;
    private int totalScoreInt;
    private Label targetScore;
    private int targetScoreInt;
    private ArrayList<String> validWordList;
    private ArrayList<String> validWordListSave;


    /**********  **********/
    public DisplayGUI(Template template) {
        this(template, -1, -1);
    }
    public DisplayGUI(Template template, int appSpecificWindowWidth, int appSpecificWindowHeight) {
        this.appSpecificWindowWidth = appSpecificWindowWidth;
        this.appSpecificWindowHeight = appSpecificWindowHeight;
        this.template = template;

    }

/********** Button **********/
    public Button initButton(String str){
        Button button = new Button();
        button.getStyleClass().add(str);
        button.setMinSize(300, 100);
        return button;
    }
/********** Scene **********/
    public void initHomeScene(){
        /*** Home Scene ***/
        exit = new Button("X");
        homePane = new BorderPane();
        sceneHome = appSpecificWindowWidth < 1 || appSpecificWindowHeight < 1 ? new Scene(homePane) : new Scene(homePane, appSpecificWindowWidth,
                appSpecificWindowHeight);

        sceneHome.getStylesheets().add("css/buzzword.css");
        homePane.getStyleClass().add("home-background");
        Label homeTitle = new Label("!Buzzword!");
        homeTitle.getStyleClass().add("heading-label");
        loginButton = initButton("login-button");
        createButton = initButton("create-button");
        quitHomeButton = initButton("quit-button");

        VBox homeWorkspace = new VBox();
        homeWorkspace.setAlignment(Pos.CENTER);
        homeWorkspace.setSpacing(50);
        loginPane = new StackPane();
        loginStack = new StackPane();
        loginPane.setMinSize(400, 200);
        loginPane.setMaxSize(400, 200);
        loginStack.setMinSize(500, 250);
        loginPane.setBackground(new Background(new BackgroundImage(new Image("images/login.png", 400, 200, false, true), BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT)));
        userNameText = new TextField("Chao");
        userNameText.setMaxWidth(200);
        passwordText = new PasswordField();
        passwordText.setMaxWidth(200);
        summitButton = new Button("Summit");
        cancelButton = new Button("Cancel");
        StackPane.setMargin(userNameText, new Insets(0, 0, 90, 145));
        StackPane.setMargin(passwordText, new Insets(0, 0, 0, 145));
        StackPane.setMargin(summitButton, new Insets(90, 0, 0, 0));
        StackPane.setMargin(cancelButton, new Insets(90, 0, 0, 290));
        loginPane.getChildren().addAll(userNameText, passwordText, summitButton, cancelButton);
        VBox blank = new VBox();
        blank.setMinSize(500, 200);
        homeWorkspace.getChildren().addAll(homeTitle, loginStack, loginButton, createButton, quitHomeButton, blank);
        homePane.setCenter(homeWorkspace);
        homePane.setRight(exit);
        // login
        homePaneLogin = new BorderPane();
        homePaneLogin.getStyleClass().add("create-background");
        userNameLabelHome = new Label("Login Already");
        userNameLabelHome.getStyleClass().add("heading-label");
        modeButton = initButton("mode-button");
        startButton = initButton("start-button");
        helpButton = initButton("help-button");
        logoutButton = initButton("logout-button");
        quitHomeLoginButton = initButton("quit-button");
        modePane = new VBox();
        modePane.setAlignment(Pos.CENTER);
        modePane.setSpacing(50);

        modeBox = new HBox[6];
        for(int i = 0; i < 6; i++){
            modeBox[i] = initModel("images/mode" + Integer.valueOf(i).toString() + ".png", template.getGameData().getModes()[i]);
        }
        modeBox[template.getGameData().getCurrentMode()].getChildren().get(1).getStyleClass().set(0, "mode-picked-label");

        VBox homeToolbar = new VBox();
        homeLoginWorkspace = new HBox();
        homeLoginWorkspace.setSpacing(30);
        homeLoginWorkspace.setAlignment(Pos.CENTER);
        homeLoginWorkspace.getChildren().addAll(homeToolbar, modePane);
        homeToolbar.setAlignment(Pos.CENTER);
        homeToolbar.setSpacing(50);
        homeToolbar.getChildren().addAll(userNameLabelHome, modeButton, startButton, helpButton, logoutButton, quitHomeLoginButton);
        homePaneLogin.setCenter(homeLoginWorkspace);


    }
    public void initCreateScene(){
        /*** Create Scene ***/
        createPane = new BorderPane();
        sceneCreate = appSpecificWindowWidth < 1 || appSpecificWindowHeight < 1 ? new Scene(createPane) : new Scene(createPane, appSpecificWindowWidth,
                appSpecificWindowHeight);
        sceneCreate.getStylesheets().add("css/buzzword.css");
        createPane.getStyleClass().add("create-background");

        createToHomeButton = initButton("home-button");
        GridPane createWorkspace = new GridPane();
        createWorkspace.setMaxSize(500, 900);
        ColumnConstraints columnLabel = new ColumnConstraints();
        columnLabel.setPercentWidth(30);
        ColumnConstraints columnText = new ColumnConstraints();
        columnText.setPercentWidth(70);
        RowConstraints rowTitle = new RowConstraints();
        rowTitle.setPercentHeight(20);
        RowConstraints rowName = new RowConstraints();
        rowName.setPercentHeight(20);
        RowConstraints rowPassword = new RowConstraints();
        rowPassword.setPercentHeight(20);
        RowConstraints rowConfirm = new RowConstraints();
        rowConfirm.setPercentHeight(20);
        RowConstraints rowToolbar = new RowConstraints();
        rowToolbar.setPercentHeight(20);
        createWorkspace.getColumnConstraints().addAll(columnLabel, columnText);
        createWorkspace.getRowConstraints().addAll(rowTitle, rowName, rowPassword, rowConfirm, rowToolbar);
        // labels
        singUp = new Label("Sing up");
        singUp.getStyleClass().add("heading-label");
        singUp.setAlignment(Pos.CENTER);
        Label name = new Label("User Account Name:");
        name.getStyleClass().add("label-sing-up");
        Label password = new Label("New Password:");
        password.getStyleClass().add("label-sing-up");
        Label confirm = new Label("Confirm Password:");
        confirm.getStyleClass().add("label-sing-up");
        HBox createToolBar = new HBox();
        // set grid pane
        createWorkspace.setConstraints(singUp, 0, 0);
        createWorkspace.setConstraints(name, 0, 1);
        createWorkspace.setConstraints(password, 0, 2);
        createWorkspace.setConstraints(confirm, 0, 3);
        createWorkspace.setConstraints(createToolBar, 0, 4);
        nameTF = new TextField();
        passNew = new PasswordField();
        passCon = new PasswordField();
        createWorkspace.setConstraints(nameTF, 1, 1);
        createWorkspace.setConstraints(passNew, 1, 2);
        createWorkspace.setConstraints(passCon, 1, 3);
        GridPane.setColumnSpan(singUp, GridPane.REMAINING);
        GridPane.setColumnSpan(createToolBar, GridPane.REMAINING);

        createToHomeButton = initButton("home-button");

        quitCreateButton = initButton("quit-button");
        createToolBar.getChildren().addAll(createToHomeButton, quitCreateButton);
        createWorkspace.getChildren().addAll(singUp, name, password, confirm, createToolBar, nameTF, passNew, passCon);
        createWorkspace.setAlignment(Pos.CENTER);
        createPane.setCenter(createWorkspace);

        /***** login *****/
        GridPane profileMatrix = new GridPane();
        profileMatrix.setAlignment(Pos.CENTER);

        HBox md[] = new HBox[6];
        profileMatrixStackPane = new StackPane[6][8];
        // reinitialize profile
        for(int i = 0; i < 6; i++){
            md[i] = initModel("images/mode" + Integer.valueOf(i) + ".png", template.getGameData().getModes()[i]);
            profileMatrix.setConstraints(md[i], 0, i);
            profileMatrix.getChildren().add(md[i]);
            for(int j = 0; j < 8; j++){
                profileMatrixStackPane[i][j] = new StackPane();
                profileMatrixStackPane[i][j].setMinSize(80, 80);
                String str = "images/level" + Integer.valueOf(j+1) + ".png";
                profileMatrixStackPane[i][j].getChildren().add(new ImageView(new Image(str, 60, 60, false, true)));
                if(j != 0) profileMatrixStackPane[i][j].getChildren().add(new ImageView(new Image("images/levelLock" + Integer.valueOf(i) + ".gif", 80, 80, false, true)));


                profileMatrixStackPane[i][j].setBackground(new Background(new BackgroundImage(new Image("images/levelUnlock.gif", 103, 103, false, true), BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT)));

                profileMatrix.setConstraints(profileMatrixStackPane[i][j], j + 1, i);
                profileMatrix.getChildren().add(profileMatrixStackPane[i][j]);
            }
        }
        //

        createLoginToHomeButton = initButton("home-button");
        createLoginToLevelButton = initButton("start-button");
        quitCreateLoginButton = initButton("quit-button");

        HBox createLoginToolBar = new HBox();
        createLoginToolBar.getChildren().addAll(createLoginToHomeButton, createLoginToLevelButton, quitCreateLoginButton);
        userNameLabelCreate = new Label();
        userNameLabelCreate.getStyleClass().add("heading-label");
        createPaneLogin = new BorderPane();
        createPaneLogin.getStyleClass().add("create-background");
        createPaneLogin.setAlignment(userNameLabelCreate, Pos.CENTER);
        createLoginToolBar.setAlignment(Pos.CENTER);
        createPaneLogin.setTop(userNameLabelCreate);
        createPaneLogin.setBottom(createLoginToolBar);
        createPaneLogin.setCenter(profileMatrix);
    }
    public void initLevelScene(){
/*** Level Scene ***/
        levelPane = new BorderPane();
        sceneLevel = appSpecificWindowWidth < 1 || appSpecificWindowHeight < 1 ? new Scene(levelPane) : new Scene(levelPane, appSpecificWindowWidth,
                appSpecificWindowHeight);
        sceneLevel.getStylesheets().add("css/buzzword.css");
        levelPane.getStyleClass().set(0, "level-background0");
        levelToHomeButton = initButton("home-button");
        profileButton = initButton("profile-button");
        quitLevelButton = initButton("quit-button");
        modeLabel = new Label("Mode : " + template.getGameData().getModes()[template.getGameData().getCurrentMode()]);
        modeLabel.getStyleClass().add("heading-label");
        HBox levelToolbar = new HBox();
        levelToolbar.setAlignment(Pos.CENTER_RIGHT);
        levelToolbar.getChildren().addAll(modeLabel, levelToHomeButton, profileButton, quitLevelButton);
        levelToolbar.setSpacing(30);
        ColumnConstraints levelColumn0 = new ColumnConstraints();
        ColumnConstraints levelColumn1 = new ColumnConstraints();
        ColumnConstraints levelColumn2 = new ColumnConstraints();
        ColumnConstraints levelColumn3 = new ColumnConstraints();
        RowConstraints levelRow0 = new RowConstraints();
        RowConstraints levelRow1 = new RowConstraints();
        RowConstraints levelRow2 = new RowConstraints();
        RowConstraints levelRow3 = new RowConstraints();
        levelMatrix = new GridPane();
        levelMatrix.setAlignment(Pos.CENTER);
        levelMatrix.getColumnConstraints().addAll(levelColumn0, levelColumn1, levelColumn2, levelColumn3);
        levelMatrix.getRowConstraints().addAll(levelRow0, levelRow1, levelRow2, levelRow3);

        levelLockImageView = new ImageView[6][4][4];
        for(int i = 0; i < 6; i++){
            for(int j = 0; j < 4; j++){
                for(int k = 0; k < 4; k++){
                    levelLockImageView[i][j][k] = new ImageView(new Image("images/levelLock" + Integer.toString(i) + ".gif", 130, 130, false, true));
                }
            }
        }


        levelCell = new StackPane[4][4];
        Random rand = new Random();
        // reinitialize level
        for(int i = 0; i < 4; i++){
            for(int j = 0; j < 4; j++){
                levelCell[i][j] = new StackPane();
                levelCell[i][j].setMinSize(130, 130);
                String str = "images/level" + Integer.valueOf(j * 4 + i + 1).toString() + ".png";
                levelCell[i][j].getChildren().add(new ImageView(new Image(str, 100, 100, false, true)));
                if(i > 0 || j > 0) levelCell[i][j].getChildren().add(levelLockImageView[0][i][j]);
                levelCell[i][j].setBackground(new Background(new BackgroundImage(new Image("images/levelUnlock.gif", 165, 165, false, true), BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT)));
                levelMatrix.add(levelCell[i][j], i, j);
            }
        }
        //
        userNameLabelLevel = new Label("Login Already");
        userNameLabelLevel.getStyleClass().add("heading-label");
        levelStack = new StackPane();
        levelStack.setMinWidth(960);
        levelStack.getChildren().addAll(levelMatrix);
        levelPane.setAlignment(userNameLabelLevel, Pos.CENTER_RIGHT);
        levelPane.setTop(userNameLabelLevel);
        levelPane.setRight(levelStack);
        levelPane.setBottom(levelToolbar);
    }
    public void initHelpScene(){
        helpPane = new BorderPane();
        sceneHelp = appSpecificWindowWidth < 1 || appSpecificWindowHeight < 1 ? new Scene(helpPane) : new Scene(helpPane, appSpecificWindowWidth,
                appSpecificWindowHeight);
        sceneHelp.getStylesheets().add("css/buzzword.css");
        helpPane.getStyleClass().add("help-background");
        ScrollBar sc = new ScrollBar();
        sc.setMin(0);
        sc.setOrientation(Orientation.VERTICAL);
        sc.setPrefHeight(720);
        sc.setMax(360);
        informationPane = new FlowPane();
        //TextField textField = new TextField(helpInfo());
        //textField.setMaxSize(1260, 720);
        //textField.setMinSize(1260, 720);
        informationPane.getChildren().addAll(sc);

        informationPane.getStyleClass().add("help-info-background");
        helpToHomeButton = initButton("home-button");
        quitHelpButton = initButton("quit-button");
        informationPane.setMaxSize(1280, 720);
        userNameLabelHelp = new Label("Login Already");
        userNameLabelHelp.getStyleClass().add("heading-label");
        HBox helpToolbar = new HBox();
        helpToolbar.getChildren().addAll(helpToHomeButton, quitHelpButton);
        helpPane.setAlignment(userNameLabelHelp, Pos.CENTER);
        helpPane.setTop(userNameLabelHelp);
        helpPane.setCenter(informationPane);
        helpPane.setBottom(helpToolbar);
        //helpPane.setStyle("-fx-opacity: 0.5;");

    }



    public void initPlayScene(){
        playPane = new BorderPane();
        scenePlay = appSpecificWindowWidth < 1 || appSpecificWindowHeight < 1 ? new Scene(playPane) : new Scene(playPane, appSpecificWindowWidth,
                appSpecificWindowHeight);
        scenePlay.getStylesheets().add("css/buzzword.css");
        playPane.getStyleClass().add("play-background");
        HBox playToolbar = new HBox();
        playToHomeButton = initButton("home-button");
        restartButton = initButton("restart-button");
        nextButton = initButton("next-button");
        pauseButton = initButton("pause-button");
        quitPlayButton = initButton("quit-button");
        playToolbar.setSpacing(105);
        playToolbar.getChildren().addAll(playToHomeButton, restartButton, pauseButton, nextButton, quitPlayButton);
        playPane.setBottom(playToolbar);
        ColumnConstraints letterColumn0 = new ColumnConstraints();
        ColumnConstraints letterColumn1 = new ColumnConstraints();
        ColumnConstraints letterColumn2 = new ColumnConstraints();
        ColumnConstraints letterColumn3 = new ColumnConstraints();
        RowConstraints letterRow0 = new RowConstraints();
        RowConstraints letterRow1 = new RowConstraints();
        RowConstraints letterRow2 = new RowConstraints();
        RowConstraints letterRow3 = new RowConstraints();
        letterMatrix = new GridPane();
        letterMatrix.getColumnConstraints().addAll(letterColumn0, letterColumn1, letterColumn2, letterColumn3);
        letterMatrix.getRowConstraints().addAll(letterRow0, letterRow1, letterRow2, letterRow3);
        letterCell = new StackPane[4][4];
        letterCellImageView = new ImageView[4][4];
        letterChoseFlag = new boolean[4][4];
        letters = new String[4][4];
        Random rand = new Random();
        for(int i = 0; i < 4; i++){
            for(int j = 0; j < 4; j++){
                letterChoseFlag[i][j] = false;
                letterCell[i][j] = new StackPane();
                letterCell[i][j].setMinSize(130, 130);
                letterCell[i][j].getChildren().add(new ImageView(new Image("images/black.jpg", 130, 130, false, true)));

                //if(rand.nextInt(3)>0) ;
                char c = (char)(65 + rand.nextInt(25));
                String str = "images/letter" + c + ".png";
                letters[i][j] = Character.toString(c);
                letterCellImageView[i][j] = new ImageView(new Image(str, 100, 100, false, true));
                letterCell[i][j].getChildren().add(letterCellImageView[i][j]);
                letterCell[i][j].setBackground(new Background(new BackgroundImage(new Image("images/letterSelected.gif", 225, 225, false, true), BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT)));
                letterMatrix.add(letterCell[i][j], i, j);
            }
        }
        letterStack = new StackPane();
        letterStack.getChildren().addAll(/*new ImageView(new Image("images/frame.png", 530, 530, false, true)),*/ letterMatrix);
        //letterStack.setMinSize(700, 700);
        //scoreStack.getChildren().add(null);
        //scoreStack.setMinSize(700, 900);
        letterMatrix.setAlignment(Pos.CENTER);
        letterMatrix.setMinWidth(650);

        Label remainTime = new Label("Time Remain :  ");
        remainTime.getStyleClass().add("timer-label");
        clock = new Label("    ");
        clock.getStyleClass().add("timer-label");

        HBox timerPane = new HBox();
        timerPane.setMinWidth(940);
        timerPane.getChildren().addAll(remainTime, clock);
        //timerPane.setMinSize(0, 150);
        //timerPane.setMaxSize(1775, 150);
        timerPane.setAlignment(Pos.CENTER_RIGHT);

        Label levelLabel = new Label("Level :  ");
        levelLabel.getStyleClass().add("level-label");
        levelDisplay = new Label("");
        levelDisplay.getStyleClass().add("level-label");

        HBox levelPaneDisplay = new HBox();
        levelPaneDisplay.setMinWidth(960);
        levelPaneDisplay.getChildren().addAll(levelLabel, levelDisplay);
        levelPaneDisplay.setAlignment(Pos.CENTER_LEFT);

        HBox topDisplay = new HBox();
        topDisplay.getChildren().addAll(levelPaneDisplay, timerPane);
        //score
        StackPane scoreStack = new StackPane();
        scoreStack.setMinWidth(650);
        BorderPane scorePane = new BorderPane();
        scorePane.getStyleClass().add("score-background");
        scorePane.setMaxSize(520, 520);
        scoreStack.getChildren().add(scorePane);
        VBox recordBox = new VBox();
        scorePane.setRight(recordBox);
        currentWordBox = new HBox();
        currentWordLabel = new Label("Current Word");
        discoveredBox = new VBox();
        totalBox = new HBox();
        totalBox.setMinSize(200, 40);
        totalBox.setMaxSize(200, 40);
        targetBox = new HBox();
        targetBox.setMinSize(200, 40);
        targetBox.setMaxSize(200, 40);
        currentWordLabel.getStyleClass().add("current-word-label");
        currentWordBox.getChildren().addAll(currentWordLabel);
        currentWordBox.setAlignment(Pos.CENTER);
        currentWordBox.setMinSize(200, 40);
        currentWordBox.setMaxSize(200, 40);
        //临时
        Label word1 = new Label("BUZZ");
        word1.getStyleClass().add("discover-word-label");
        Label score1 = new Label("10");
        score1.getStyleClass().add("discover-word-label");
        HBox wordBox1 = new HBox();
        wordBox1.setAlignment(Pos.CENTER);
        wordBox1.setSpacing(20);
        wordBox1.getChildren().addAll(word1, score1);

        Label word2 = new Label("WORD");
        word2.getStyleClass().add("discover-word-label");
        Label score2 = new Label("10");
        score2.getStyleClass().add("discover-word-label");
        HBox wordBox2 = new HBox();
        wordBox2.setAlignment(Pos.CENTER);
        wordBox2.setSpacing(20);
        wordBox2.getChildren().addAll(word2, score2);

        discoveredBox.getChildren().addAll(wordBox1, wordBox2);
        //临时
        discoveredBox.setMinSize(200,400);
        discoveredBox.setMaxSize(200,400);
        Label totalLabel = new Label("Total: ");
        totalLabel.getStyleClass().add("total-word-label");
        totalScore = new Label("70");
        totalScore.getStyleClass().add("total-word-label");
        Label targetLabel = new Label("Target: ");
        targetLabel.getStyleClass().add("target-word-label");
        targetScore = new Label("100");
        targetScore.getStyleClass().add("target-word-label");
        totalBox.getChildren().addAll(totalLabel, totalScore);
        totalBox.setAlignment(Pos.CENTER);
        totalBox.setSpacing(20);
        targetBox.getChildren().addAll(targetLabel, targetScore);
        targetBox.setAlignment(Pos.CENTER);
        targetBox.setSpacing(20);
        recordBox.getChildren().addAll(currentWordBox, discoveredBox, totalBox, targetBox);
        scorePane.setRight(recordBox);
        recordBox.setMinWidth(200);
        //recordBox.setStyle("-fx-background-color: blue");
        validWordList = new ArrayList<>();
        validWordListSave = new ArrayList<>();

        playPane.setTop(topDisplay);
        playPane.setRight(letterStack);
        playPane.setLeft(scoreStack);
    }
    public void initializeScene(){
        initHomeScene();
        initCreateScene();
        initLevelScene();
        initHelpScene();
        initPlayScene();
    }
    public void initializeHandlers(){
        Controller controller = template.getController();//new HangmanController(app, startGame);
        exit.setOnMouseClicked(e -> controller.handleExit());
        quitHomeButton.setOnMouseClicked(e -> controller.handleExit());
        quitCreateButton.setOnMouseClicked(e -> controller.handleExit());
        quitCreateLoginButton.setOnMouseClicked(e -> controller.handleExit());
        quitPlayButton.setOnMouseClicked(e -> controller.handleExitPause());
        quitLevelButton.setOnMouseClicked(e -> controller.handleExit());
        quitHelpButton.setOnMouseClicked(e -> controller.handleExit());
        createButton.setOnMouseClicked(e -> controller.handleCreate());
        profileButton.setOnMouseClicked(e -> controller.handleCreate());
        loginButton.setOnMouseClicked(e -> controller.handleLogin());
        createToHomeButton.setOnMouseClicked(e -> controller.handleHome());
        createLoginToHomeButton.setOnMouseClicked(e -> controller.handleHome());
        helpToHomeButton.setOnMouseClicked(e -> controller.handleHome());
        playToHomeButton.setOnMouseClicked(e -> controller.handleHome());
        levelToHomeButton.setOnMouseClicked(e -> controller.handleHome());
        pauseButton.setOnMouseClicked(e -> controller.handlePause());
        summitButton.setOnMouseClicked(e -> controller.handleSummit());
        cancelButton.setOnMouseClicked(e -> controller.handleCancel());
        //modeButton.setOnMouseClicked(e -> controller.handleMode());
        startButton.setOnMouseClicked(e -> controller.handleStart());
        createLoginToLevelButton.setOnMouseClicked(e -> controller.handleStart());
        helpButton.setOnMouseClicked(e -> controller.handleHelp());
        logoutButton.setOnMouseClicked(e -> controller.handleLogout());
        quitHomeLoginButton.setOnMouseClicked(e -> controller.handleExit());
        //levelMatrix.setOnMouseClicked(e -> controller.handlePlay());
        modeButton.setOnMouseClicked(e -> controller.handleModePick());
        singUp.setOnMouseClicked(e -> controller.handleSignUp());
        restartButton.setOnMouseClicked(e -> controller.handleRestart());
        nextButton.setOnMouseClicked(e -> controller.handleNextLevel());
/***** mode choose handle *****/
        modeBox[0].setOnMouseClicked(e -> controller.handleMode(0));
        modeBox[1].setOnMouseClicked(e -> controller.handleMode(1));
        modeBox[2].setOnMouseClicked(e -> controller.handleMode(2));
        modeBox[3].setOnMouseClicked(e -> controller.handleMode(3));
        modeBox[4].setOnMouseClicked(e -> controller.handleMode(4));
        modeBox[5].setOnMouseClicked(e -> controller.handleMode(5));
/***** level choose handle *****/
        levelCell[0][0].setOnMouseClicked(e -> controller.handlePlay(0, 0));
        levelCell[0][1].setOnMouseClicked(e -> controller.handlePlay(0, 1));
        levelCell[0][2].setOnMouseClicked(e -> controller.handlePlay(0, 2));
        levelCell[0][3].setOnMouseClicked(e -> controller.handlePlay(0, 3));
        levelCell[1][0].setOnMouseClicked(e -> controller.handlePlay(1, 0));
        levelCell[1][1].setOnMouseClicked(e -> controller.handlePlay(1, 1));
        levelCell[1][2].setOnMouseClicked(e -> controller.handlePlay(1, 2));
        levelCell[1][3].setOnMouseClicked(e -> controller.handlePlay(1, 3));
        levelCell[2][0].setOnMouseClicked(e -> controller.handlePlay(2, 0));
        levelCell[2][1].setOnMouseClicked(e -> controller.handlePlay(2, 1));
        levelCell[2][2].setOnMouseClicked(e -> controller.handlePlay(2, 2));
        levelCell[2][3].setOnMouseClicked(e -> controller.handlePlay(2, 3));
        levelCell[3][0].setOnMouseClicked(e -> controller.handlePlay(3, 0));
        levelCell[3][1].setOnMouseClicked(e -> controller.handlePlay(3, 1));
        levelCell[3][2].setOnMouseClicked(e -> controller.handlePlay(3, 2));
        levelCell[3][3].setOnMouseClicked(e -> controller.handlePlay(3, 3));
/***** letter choose handle *****/
        //levelMatrix.setOnDragDetected(e -> levelMatrix.startFullDrag());
        //levelMatrix.setOnMouseDragReleased(e -> controller.handleLetterMatrixMouseReleased());
/*
        letterCell[0][0].setOnMouseClicked(e -> controller.handleLetterChoose(0, 0));
        letterCell[0][1].setOnMouseClicked(e -> controller.handleLetterChoose(0, 1));
        letterCell[0][2].setOnMouseClicked(e -> controller.handleLetterChoose(0, 2));
        letterCell[0][3].setOnMouseClicked(e -> controller.handleLetterChoose(0, 3));
        letterCell[1][0].setOnMouseClicked(e -> controller.handleLetterChoose(1, 0));
        letterCell[1][1].setOnMouseClicked(e -> controller.handleLetterChoose(1, 1));
        letterCell[1][2].setOnMouseClicked(e -> controller.handleLetterChoose(1, 2));
        letterCell[1][3].setOnMouseClicked(e -> controller.handleLetterChoose(1, 3));
        letterCell[2][0].setOnMouseClicked(e -> controller.handleLetterChoose(2, 0));
        letterCell[2][1].setOnMouseClicked(e -> controller.handleLetterChoose(2, 1));
        letterCell[2][2].setOnMouseClicked(e -> controller.handleLetterChoose(2, 2));
        letterCell[2][3].setOnMouseClicked(e -> controller.handleLetterChoose(2, 3));
        letterCell[3][0].setOnMouseClicked(e -> controller.handleLetterChoose(3, 0));
        letterCell[3][1].setOnMouseClicked(e -> controller.handleLetterChoose(3, 1));
        letterCell[3][2].setOnMouseClicked(e -> controller.handleLetterChoose(3, 2));
        letterCell[3][3].setOnMouseClicked(e -> controller.handleLetterChoose(3, 3));
*/


        letterCell[0][0].setOnDragDetected(e -> { letterCell[0][0].startFullDrag();});
        letterCell[0][1].setOnDragDetected(e -> { letterCell[0][1].startFullDrag();});
        letterCell[0][2].setOnDragDetected(e -> { letterCell[0][2].startFullDrag();});
        letterCell[0][3].setOnDragDetected(e -> { letterCell[0][3].startFullDrag();});
        letterCell[1][0].setOnDragDetected(e -> { letterCell[1][0].startFullDrag();});
        letterCell[1][1].setOnDragDetected(e -> { letterCell[1][1].startFullDrag();});
        letterCell[1][2].setOnDragDetected(e -> { letterCell[1][2].startFullDrag();});
        letterCell[1][3].setOnDragDetected(e -> { letterCell[1][3].startFullDrag();});
        letterCell[2][0].setOnDragDetected(e -> { letterCell[2][0].startFullDrag();});
        letterCell[2][1].setOnDragDetected(e -> { letterCell[2][1].startFullDrag();});
        letterCell[2][2].setOnDragDetected(e -> { letterCell[2][2].startFullDrag();});
        letterCell[2][3].setOnDragDetected(e -> { letterCell[2][3].startFullDrag();});
        letterCell[3][0].setOnDragDetected(e -> { letterCell[3][0].startFullDrag();});
        letterCell[3][1].setOnDragDetected(e -> { letterCell[3][1].startFullDrag();});
        letterCell[3][2].setOnDragDetected(e -> { letterCell[3][2].startFullDrag();});
        letterCell[3][3].setOnDragDetected(e -> { letterCell[3][3].startFullDrag();});


        letterCell[0][0].setOnMouseDragEntered(e -> controller.handleLetterChoose(0, 0));
        letterCell[0][1].setOnMouseDragEntered(e -> controller.handleLetterChoose(0, 1));
        letterCell[0][2].setOnMouseDragEntered(e -> controller.handleLetterChoose(0, 2));
        letterCell[0][3].setOnMouseDragEntered(e -> controller.handleLetterChoose(0, 3));
        letterCell[1][0].setOnMouseDragEntered(e -> controller.handleLetterChoose(1, 0));
        letterCell[1][1].setOnMouseDragEntered(e -> controller.handleLetterChoose(1, 1));
        letterCell[1][2].setOnMouseDragEntered(e -> controller.handleLetterChoose(1, 2));
        letterCell[1][3].setOnMouseDragEntered(e -> controller.handleLetterChoose(1, 3));
        letterCell[2][0].setOnMouseDragEntered(e -> controller.handleLetterChoose(2, 0));
        letterCell[2][1].setOnMouseDragEntered(e -> controller.handleLetterChoose(2, 1));
        letterCell[2][2].setOnMouseDragEntered(e -> controller.handleLetterChoose(2, 2));
        letterCell[2][3].setOnMouseDragEntered(e -> controller.handleLetterChoose(2, 3));
        letterCell[3][0].setOnMouseDragEntered(e -> controller.handleLetterChoose(3, 0));
        letterCell[3][1].setOnMouseDragEntered(e -> controller.handleLetterChoose(3, 1));
        letterCell[3][2].setOnMouseDragEntered(e -> controller.handleLetterChoose(3, 2));
        letterCell[3][3].setOnMouseDragEntered(e -> controller.handleLetterChoose(3, 3));

        letterCell[0][0].setOnMouseDragReleased(e -> controller.handleLetterMatrixMouseReleased());
        letterCell[0][1].setOnMouseDragReleased(e -> controller.handleLetterMatrixMouseReleased());
        letterCell[0][2].setOnMouseDragReleased(e -> controller.handleLetterMatrixMouseReleased());
        letterCell[0][3].setOnMouseDragReleased(e -> controller.handleLetterMatrixMouseReleased());
        letterCell[1][0].setOnMouseDragReleased(e -> controller.handleLetterMatrixMouseReleased());
        letterCell[1][1].setOnMouseDragReleased(e -> controller.handleLetterMatrixMouseReleased());
        letterCell[1][2].setOnMouseDragReleased(e -> controller.handleLetterMatrixMouseReleased());
        letterCell[1][3].setOnMouseDragReleased(e -> controller.handleLetterMatrixMouseReleased());
        letterCell[2][0].setOnMouseDragReleased(e -> controller.handleLetterMatrixMouseReleased());
        letterCell[2][1].setOnMouseDragReleased(e -> controller.handleLetterMatrixMouseReleased());
        letterCell[2][2].setOnMouseDragReleased(e -> controller.handleLetterMatrixMouseReleased());
        letterCell[2][3].setOnMouseDragReleased(e -> controller.handleLetterMatrixMouseReleased());
        letterCell[3][0].setOnMouseDragReleased(e -> controller.handleLetterMatrixMouseReleased());
        letterCell[3][1].setOnMouseDragReleased(e -> controller.handleLetterMatrixMouseReleased());
        letterCell[3][2].setOnMouseDragReleased(e -> controller.handleLetterMatrixMouseReleased());
        letterCell[3][3].setOnMouseDragReleased(e -> controller.handleLetterMatrixMouseReleased());

/***** keys shortcut *****/
        sceneHome.setOnKeyPressed((KeyEvent event) -> controller.handelHomeKeyShortcut(true, event));
        sceneHome.setOnKeyReleased((KeyEvent event) -> controller.handelHomeKeyShortcut(false, event));
    }
    public void initializeStage(){
        initializeScene();
        initializeHandlers();
        //Parent root = (Parent) FXMLLoader.load(getClass().getResource("sample.fxml"));



        primaryStage = new Stage();
        primaryStage.setTitle("Dota2 Style Buzzword");


        primaryStage.setMaximized(true);
        primaryStage.setScene(sceneHome);
        primaryStage.initStyle(StageStyle.TRANSPARENT);
        primaryStage.show();


        //Transition&#x5171;&#x6709;10&#x4e2a;&#x5b50;&#x7c7b;&#xff0c;&#x5206;&#x522b;&#x662f;&#xff1a;FadeTransition, FillTransition, ParallelTransition, PathTransition, PauseTransition, RotateTransition, ScaleTransition, SequentialTransition, StrokeTransition, TranslateTransition&#x3002;
        /*
        final Circle circle = new Circle(20, 20, 15);
        circle.setFill(Color.DARKRED);
        final Path path = new Path();
        path.getElements().add(new MoveTo(20, 20));
        path.getElements().add(new CubicCurveTo(30, 10, 380, 120, 200, 120));
        path.getElements().add(new CubicCurveTo(200, 1120, 110, 240, 380, 240));
        path.setOpacity(0.5);
        homePane.getChildren().add(path);
        homePane.getChildren().add(circle);
        final PathTransition pathTransition = new PathTransition();
        pathTransition.setDuration(Duration.seconds(8.0));
        pathTransition.setDelay(Duration.seconds(0.5));
        pathTransition.setPath(path);
        pathTransition.setNode(circle);
        pathTransition.setOrientation(PathTransition.OrientationType.ORTHOGONAL_TO_TANGENT);
        pathTransition.setCycleCount(Timeline.INDEFINITE);
        pathTransition.setAutoReverse(true);
        pathTransition.play();
        */

    }

    public HBox initModel(String image, String text){
        HBox hbox = new HBox();
        Label label = new Label(text);
        label.getStyleClass().add("mode-label");
        hbox.getChildren().addAll(new ImageView(new Image(image, 142, 80, false, true)), label);
        hbox.setSpacing(20);
        return hbox;
    }

    public void resetLoginLabels(String userName){
        userNameLabelCreate.setText(userName);
        userNameLabelHelp.setText(userName);
        userNameLabelHome.setText(userName);
        userNameLabelLevel.setText(userName);

    }
    private String helpInfo() {
        String str = "";
        return str;
    }
/********** Accessor **********/
    public Template getTemplate() {
        return template;
    }

    public Stage getPrivateStage() {
        return primaryStage;
    }

    public Scene getSceneHome() {
        return sceneHome;
    }

    public Scene getSceneCreate() {
        return sceneCreate;
    }

    public Scene getSceneHelp() {
        return sceneHelp;
    }

    public Scene getSceneLevel() {
        return sceneLevel;
    }

    public Scene getScenePlay() {
        return scenePlay;
    }

    public Button getPauseButton() {
        return pauseButton;
    }

    public StackPane getLetterStack() {
        return letterStack;
    }

    public ImageView getShell() {
        return shell;
    }

    public Label getClock() {
        return clock;
    }

    public StackPane getLoginPane() {
        return loginPane;
    }

    public StackPane getLoginStack() {
        return loginStack;
    }

    public BorderPane getHomePaneLogin() {
        return homePaneLogin;
    }

    public BorderPane getHomePane() {
        return homePane;
    }

    public VBox getModePane() {
        return modePane;
    }

    public TextField getNameTF() {
        return nameTF;
    }

    public PasswordField getPassNew() {
        return passNew;
    }

    public PasswordField getPassCon() {
        return passCon;
    }

    public Label getUserNameLabelCreate() {
        return userNameLabelCreate;
    }

    public BorderPane getCreatePaneLogin() {
        return createPaneLogin;
    }

    public StackPane[][] getProfileMatrixStackPane() {
        return profileMatrixStackPane;
    }

    public TextField getUserNameText() {
        return userNameText;
    }

    public PasswordField getPasswordText() {
        return passwordText;
    }

    public StackPane[][] getLevelCell() {
        return levelCell;
    }

    public HBox[] getModeBox() {
        return modeBox;
    }

    public Label getModeLabel() {
        return modeLabel;
    }

    public BorderPane getCreatePane() {
        return createPane;
    }

    public Label getLevelDisplay() {
        return levelDisplay;
    }

    public String[][] getLetters() {
        return letters;
    }

    public boolean[][] getLetterChoseFlag() {
        return letterChoseFlag;
    }

    public StackPane[][] getLetterCell() {
        return letterCell;
    }

    public BorderPane getLevelPane() {
        return levelPane;
    }

    public ImageView[][][] getLevelLockImageView() {
        return levelLockImageView;
    }

    public Label getTotalScore() {
        return totalScore;
    }

    public int getTotalScoreInt() {
        return totalScoreInt;
    }

    public Label getTargetScore() {
        return targetScore;
    }

    public int getTargetScoreInt() {
        return targetScoreInt;
    }

    public void setTargetScoreInt(int targetScoreInt) {
        this.targetScoreInt = targetScoreInt;
    }

    public ArrayList<String> getValidWordList() {
        return validWordList;
    }

    public ArrayList<String> getValidWordListSave() {
        return validWordListSave;
    }

    public void setTotalScoreInt(int totalScoreInt) {
        this.totalScoreInt = totalScoreInt;
    }

    public Label getCurrentWordLabel() {
        return currentWordLabel;
    }

    public VBox getDiscoveredBox() {
        return discoveredBox;
    }

    public Button getNextButton() {
        return nextButton;
    }
}
