package buzzword;

import controller.Controller;
import controller.PropertyManager;
import data.FileManager;
import data.GameData;
import data.LevelContent;
import data.UserList;
import gui.DisplayGUI;
import javafx.application.Application;
import javafx.stage.Stage;

public class Buzzword extends Application {
    private final PropertyManager propertyManager = PropertyManager.getManager();

    @Override
    public void start(Stage primaryStage) throws Exception{
        Template template = new Template();
        Controller controller = new Controller(template);
        GameData gameData = new GameData(template);
        UserList userList = new UserList(template);
        FileManager fileManger = new FileManager(template);
        LevelContent levelContent = new LevelContent(template);
        DisplayGUI displayGUI = new DisplayGUI(template, 1920, 1080);
        template.setController(controller);
        template.setGameData(gameData);
        template.setUserList(userList);
        template.setFileManager(fileManger);
        template.setLevelContent(levelContent);
        template.setDisplayGUI(displayGUI);

        //fileManger.loadUserList();



        template.getDisplayGUI().initializeStage();
        template.getController().start();







    }


    public static void main(String[] args) {
        launch(args);
    }
}
