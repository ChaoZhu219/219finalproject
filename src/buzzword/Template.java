package buzzword;
// packages
import controller.PropertyManager;
import controller.Controller;
import data.FileManager;
import data.GameData;
import data.LevelContent;
// java FX

import data.UserList;
import gui.DisplayGUI;

/**
 * Created by peter on 2016/10/30.
 */
public class Template {
/********** Member Variable **********/
    private final PropertyManager propertyManger = PropertyManager.getManager();
    private Controller controller;
    private GameData gameData;
    private UserList userList;
    private FileManager fileManager;
    private LevelContent levelContent;
    private DisplayGUI displayGUI;
/********** Member Variable **********/
    public Template() {

    }


/********** Accessor **********/
    public PropertyManager getPropertyManger() {
        return propertyManger;
    }

    public Controller getController() {
        return controller;
    }

    public void setController(Controller controller) {
        this.controller = controller;
    }

    public GameData getGameData() {
        return gameData;
    }

    public void setGameData(GameData gameData) {
        this.gameData = gameData;
    }

    public FileManager getFileManager() {
        return fileManager;
    }

    public void setFileManager(FileManager fileManger) {
        this.fileManager = fileManger;
    }

    public LevelContent getLevelContent() {
        return levelContent;
    }

    public void setLevelContent(LevelContent levelContent) {
        this.levelContent = levelContent;
    }

    public DisplayGUI getDisplayGUI() {
        return displayGUI;
    }

    public void setDisplayGUI(DisplayGUI displayGUI) {
        this.displayGUI = displayGUI;
    }

    public UserList getUserList() {
        return userList;
    }

    public void setUserList(UserList userList) {
        this.userList = userList;
    }
}
