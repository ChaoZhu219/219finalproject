package data;

import buzzword.Template;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

/**
 * Created by peter on 2016/10/30.
 */
public class LevelContent {
    private static final int WORDS_NUMBER = 330581;
    private Template template;
    private String[] validWords;
    private HashSet<String>[] wordDictionary;

    public LevelContent(Template template) {
        this.template = template;
        wordDictionary = new HashSet[6];
        for(int i = 0; i < 6; i++){
            wordDictionary[i] = new HashSet<>();
        }
        //URL wordRef = getClass().getClassLoader().getResource("data/words0.txt");
        File[] file = new File[6];
        for(int i = 0; i < 6; i++){
            file[i] = new File("resources/data/words" + i + ".txt");
            try(Scanner scanner = new Scanner(file[i])){
                while(scanner.hasNextLine()){
                    wordDictionary[i].add(scanner.nextLine());
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }








        initialize();
        /*
        for(int i = 0; i < validWords.length; i++){
            System.out.println(validWords[i].length());
        }
        */
    }



    public void initialize(){
        validWords = new String[]{
                "EARTHSHAKER",
                "SVEN",
                "TINY",
                "KUNKKA",
                "BEASTMASTER",
                "DRAGON KNIGHT",
                "CLOCKWERK",
                "OMNIKNIGHT",
                "HUSKAR",
                "ALCHEMIST",
                "BREWMASTER",
                "TREANT PROTECTOR",
                "I O",
                "CENTAUR",
                "TIMBERSAW",
                "BRISTLEBACK",
                "TUSK",
                "ELDER TITAN",
                "LEGION COMMANDER",
                "EARTH SPIRIT",
                "PHOENIX",

                "AXE",
                "PUDGE",
                "SAND KING",
                "SLARDAR",
                "TIDEHUNTER",
                "WRAITH KING",
                "LIFESTEALER",
                "NIGHT STALKER",
                "DOOM",
                "SPIRIT BREAKER",
                "LYCAN",
                "CHAOS KNIGHT",
                "UNDYING",
                "MAGNUS",
                "ABADDON",
                "UNDERLORD",

                "ANTI MAGE",
                "DROW RANGER",
                "JUGGERNAUT",
                "MIRANA",
                "MORPHLING",
                "PHANTOM LANCER",
                "VENGEFUL SPIRIT",
                "RIKI",
                "SNIPER",
                "TEMPLAR ASSASSIN",
                "LUNA",
                "BOUNTY HUNTER",
                "URSA",
                "GYROCOPTER",
                "LONE DRUID",
                "NAGA SIREN",
                "TROLL WARLORD",
                "EMBER SPIRIT",

                "BLOODSEEKER",
                "SHADOW FIEND",
                "RAZOR",
                "VENOMANCER",
                "FACELESS VOID",
                "PHANTOM ASSASSIN",
                "VIPER",
                "CLINKZ",
                "BROODMOTHER",
                "WEAVER",
                "SPECTRE",
                "MEEPO",
                "NYX ASSASSIN",
                "SLARK",
                "MEDUSA",
                "TERRORBLADE",
                "ARC WARDEN",

                "CRYSTAL MAIDEN",
                "PUCK",
                "STORM SPIRIT",
                "WINDRANGER",
                "ZEUS",
                "LINA",
                "SHADOW SHAMAN",
                "TINKER",
                "NATURE PROPHET",
                "ENCHANTRESS",
                "JAKIRO",
                "CHEN",
                "SILENCER",
                "OGRE MAGI",
                "RUBICK",
                "DISRUPTOR",
                "KOTL",
                "SKYWRATH MAGE",
                "ORACLE",
                "TECHIES",

                "BANE",
                "LICH",
                "LION",
                "WITCH DOCTOR",
                "ENIGMA",
                "NECROPHOS",
                "WARLOCK",
                "QUEEN OF PAIN",
                "DEATH PROPHET",
                "PUGNA",
                "DAZZLE",
                "LESHRAC",
                "DARK SEER",
                "BATRIDER",
                "APPARITION",
                "INVOKER",
                "O D",
                "SHADOW DEMON",
                "VISAGE",
                "WINTER WYVERN"
        };
    }

    /***** getter and setter ******/
    public String[] getValidWords() {
        return validWords;
    }

    public HashSet<String>[] getWordDictionary() {
        return wordDictionary;
    }
}
