package data;

import buzzword.Template;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by peter on 2016/10/30.
 */
public class FileManager {
    private Template template;
    private static final Path path = Paths.get("H:\\Stony Brook Fall 2016\\CSE 219\\HW3toFinal\\Buzzword\\resources\\data");

    public FileManager(Template template) {
        this.template = template;
    }



    public GameData loadUserData(GameData gameData, String userName) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        try{
            String temp = path + "\\" + userName + ".json";
            //gameData = mapper.readValue(new File(path + "\\" + userName + ".json"), GameData.class);
            gameData = mapper.readValue(new File(path + "\\" + userName + ".json"), GameData.class);
            gameData.setTemplate(template);
            return gameData;
        }
        catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    public void saveUserData(){
        ObjectMapper mapper = new ObjectMapper();
        try{
            mapper.writeValue(new File(path  + "\\" + template.getGameData().getUserName() + ".json"), template.getGameData());
        }
        catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void saveUserList() throws IOException{
        ObjectMapper mapper = new ObjectMapper();
        try{
            mapper.writeValue(new File(path + "\\UserList.json"), template.getUserList());
        }
        catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public UserList loadUserList(UserList userList) throws IOException{
        ObjectMapper mapper = new ObjectMapper();
        try{
            userList = mapper.readValue(new File(path + "\\UserList.json"), UserList.class);
            userList.setTemplate(template);
            return userList;
        }
        catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
