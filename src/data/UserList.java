package data;

import buzzword.Template;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by peter on 2016/11/13.
 */
public class UserList {

    @JsonIgnore
    private Template template;
    @JsonProperty
    private int numberOfUser;
    @JsonProperty
    private ArrayList<String> userName;
    @JsonProperty
    private ArrayList<String> userPassword;

    public UserList(Template template) {
        this.template = template;
    }
    public UserList(){

    }

    public void createNewUserList(Template template) throws IOException {
        this.template = template;
        this.numberOfUser = 1;
        userName = new ArrayList<>();
        userPassword = new ArrayList<>();
        userName.add("Chao");
        userPassword.add("1234");
        template.getFileManager().saveUserList();
    }

    public void addNewUser(String newName, String newPassword) throws IOException {
        numberOfUser++;
        userName.add(newName);
        userPassword.add(newPassword);
        template.getFileManager().saveUserList();
    }

    /***** getter and setter *****/
    public Template getTemplate() {
        return template;
    }

    public void setTemplate(Template template) {
        this.template = template;
    }

    public int getNumberOfUser() {
        return numberOfUser;
    }

    public void setNumberOfUser(int numberOfUser) {
        this.numberOfUser = numberOfUser;
    }

    public ArrayList<String> getUserName() {
        return userName;
    }

    public void setUserName(ArrayList<String> userName) {
        this.userName = userName;
    }

    public ArrayList<String> getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(ArrayList<String> userPassword) {
        this.userPassword = userPassword;
    }
}
