package data;

import buzzword.Template;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by peter on 2016/10/29.
 */
public class GameData {
    @JsonIgnore
    private Template template;
    @JsonIgnore
    private static final String[] modes = {"Hero", "Color", "Fruit", "Animal", "Major", "GRE"};
    @JsonIgnore
    private int currentMode = 0;
    @JsonIgnore
    private int currentLevel = 1;
    @JsonProperty
    private String userName;
    @JsonProperty
    private boolean[][] modeLevelFlag;
    @JsonProperty
    private int[][] personalBest;

    public GameData(Template template) {
        this.template = template;
    }
    public GameData(){

    }
    public void createNewGameData(Template template, String name) throws IOException {
        this.template = template;
        modeLevelFlag = new boolean[6][16];
        personalBest = new int[6][16];
        for(int i = 0; i < 6; i++){
            for(int j = 0; j < 15; j++){
                personalBest[i][j] = 0;
                modeLevelFlag[i][j] = false;
                if(j == 0) modeLevelFlag[i][j] = true;
            }
        }
        userName = name;
        template.getFileManager().saveUserData();
    }




    /***** getter and setter *****/
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public static String[] getModes() {
        return modes;
    }

    public void setTemplate(Template template) {
        this.template = template;
    }

    public boolean[][] getModeLevelFlag() {
        return modeLevelFlag;
    }

    public int getCurrentMode() {
        return currentMode;
    }

    public int getCurrentLevel() {
        return currentLevel;
    }

    public void setCurrentMode(int currentMode) {
        this.currentMode = currentMode;
    }

    public void setCurrentLevel(int currentLevel) {
        this.currentLevel = currentLevel;
    }

    public int[][] getPersonalBest() {
        return personalBest;
    }
}
